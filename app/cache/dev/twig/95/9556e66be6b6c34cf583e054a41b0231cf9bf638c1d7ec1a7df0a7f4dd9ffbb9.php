<?php

/* base.html.twig */
class __TwigTemplate_82ca5f261402cf881df25ec8ac94ed1447debb89dd86aee45f9fd17180a4b48b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
                <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width\">
";
        // line 12
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "48ed673_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_48ed673_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/48ed673_comments_1.css");
            // line 13
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
            echo "\">
        <link rel=\"stylesheet\" href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/icomoon-social.css"), "html", null, true);
            echo "\">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

        <link rel=\"stylesheet\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.css"), "html", null, true);
            echo "\" />
\t\t<!--[if lte IE 8]>
\t\t    <link rel=\"stylesheet\" href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.ie.css"), "html", null, true);
            echo "\" />
\t\t<![endif]-->
\t\t<link rel=\"stylesheet\" href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
            echo "\">

        <script src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
            echo "\"></script>
        <script src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "48ed673"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_48ed673") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/48ed673.css");
            // line 13
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
            echo "\">
        <link rel=\"stylesheet\" href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/icomoon-social.css"), "html", null, true);
            echo "\">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

        <link rel=\"stylesheet\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.css"), "html", null, true);
            echo "\" />
\t\t<!--[if lte IE 8]>
\t\t    <link rel=\"stylesheet\" href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.ie.css"), "html", null, true);
            echo "\" />
\t\t<![endif]-->
\t\t<link rel=\"stylesheet\" href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
            echo "\">

        <script src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
            echo "\"></script>
        <script src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
        // line 26
        echo "        ";
        $this->displayBlock('css', $context, $blocks);
        // line 28
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 31
        $this->loadTemplate("::navbar.html.twig", "base.html.twig", 31)->display($context);
        // line 32
        echo "        
        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 39
        echo "        
        
        
        
        
        ";
        // line 44
        $this->loadTemplate("::footer.html.twig", "base.html.twig", 44)->display($context);
        // line 45
        echo "        
        
        ";
        // line 47
        $this->displayBlock('javascripts', $context, $blocks);
        // line 49
        echo "          <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}\"></script>
        <script>window.jQuery || document.write('<script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.min.js"), "html", null, true);
        echo "\"><\\/script>')</script>
        <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js')}}\"></script>
        <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.fitvids.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.sequence-min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.bxslider.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/main-menu.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/template.js"), "html", null, true);
        echo "\"></script>
        
        
        
    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        // line 27
        echo "        ";
    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        // line 34
        echo "        
        
        
        
        ";
    }

    // line 47
    public function block_javascripts($context, array $blocks = array())
    {
        // line 48
        echo "        ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 48,  211 => 47,  203 => 34,  200 => 33,  196 => 27,  193 => 26,  187 => 5,  176 => 57,  172 => 56,  168 => 55,  164 => 54,  160 => 53,  155 => 51,  151 => 50,  148 => 49,  146 => 47,  142 => 45,  140 => 44,  133 => 39,  131 => 33,  128 => 32,  126 => 31,  119 => 28,  116 => 26,  110 => 24,  106 => 23,  101 => 21,  96 => 19,  91 => 17,  85 => 14,  80 => 13,  73 => 24,  69 => 23,  64 => 21,  59 => 19,  54 => 17,  48 => 14,  43 => 13,  39 => 12,  29 => 5,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         */
/*                 <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">*/
/*         <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>*/
/*         <meta name="description" content="">*/
/*         <meta name="viewport" content="width=device-width">*/
/* {% stylesheets '@FOSCommentBundle/Resources/assets/css/comments.css' %}*/
/*         <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/icomoon-social.css')}}">*/
/*         <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" />*/
/* 		<!--[if lte IE 8]>*/
/* 		    <link rel="stylesheet" href="{{asset('css/leaflet.ie.css')}}" />*/
/* 		<![endif]-->*/
/* 		<link rel="stylesheet" href="{{asset('css/main.css')}}">*/
/* */
/*         <script src="{{asset('js/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>*/
/*         <script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js')}}"></script>*/
/* {% endstylesheets %}*/
/*         {% block css %}*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% include '::navbar.html.twig' %}*/
/*         */
/*         {% block body %}*/
/*         */
/*         */
/*         */
/*         */
/*         {% endblock %}*/
/*         */
/*         */
/*         */
/*         */
/*         */
/*         {% include '::footer.html.twig' %}*/
/*         */
/*         */
/*         {% block javascripts %}*/
/*         {% endblock %}*/
/*           <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}"></script>*/
/*         <script>window.jQuery || document.write('<script src="{{asset('js/jquery-1.9.1.min.js')}}"><\/script>')</script>*/
/*         <script src="{{asset('js/bootstrap.min.js')}}"></script>*/
/*         <script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.fitvids.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.sequence-min.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.bxslider.js')}}"></script>*/
/*         <script src="{{asset('js/main-menu.js')}}"></script>*/
/*         <script src="{{asset('js/template.js')}}"></script>*/
/*         */
/*         */
/*         */
/*     </body>*/
/* </html>*/
/* */

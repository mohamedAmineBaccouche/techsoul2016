<?php

/* utilisateurBundle:Projet:show.html.twig */
class __TwigTemplate_a36797e62fd034a0e3c98c26270e2a2f5320e49fc73389cbbf931a90bc9c2b9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Projet:show.html.twig", 3);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_css($context, array $blocks = array())
    {
        // line 5
        echo "

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "    <!-- End Product Image & Available 
    <h1>Projet</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Idprojet</th>
                <td>";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "idProjet", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nomprojet</th>
                <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nomProjet", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Resume</th>
                <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "resume", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Image</th>
                <td>";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "image", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Budjet</th>
                <td>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "budjet", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Fichier</th>
                <td>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fichier", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "idProjet", array()))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 55
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
    
    Colors -->
    
    
    
    
    
    
    
    <!-- Page Title -->
\t\t<div class=\"section section-breadcrumbs\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h1>";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nomProjet", array()), "html", null, true);
        echo "</h1>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
        
        <div class=\"section\">
\t    \t<div class=\"container\">
\t    \t\t<div class=\"row\">
\t    \t\t\t<!-- Product Image & Available Colors -->
\t    \t\t\t<div class=\"col-sm-6\">
\t    \t\t\t\t<div class=\"product-image-large\">
\t    \t\t\t\t\t<img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/image/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "imageFile", array()), "html", null, true);
        echo "\" alt=\"Item Name\">
\t    \t\t\t\t</div>
\t    \t\t\t\t<div class=\"colors\">
\t\t\t\t\t\t\t<span class=\"color-white\"></span>
\t\t\t\t\t\t\t<span class=\"color-black\"></span>
\t\t\t\t\t\t\t<span class=\"color-blue\"></span>
\t\t\t\t\t\t\t<span class=\"color-orange\"></span>
\t\t\t\t\t\t\t<span class=\"color-green\"></span>
\t\t\t\t\t\t</div>
\t    \t\t\t</div>
\t    \t\t\t<!-- End Product Image & Available Colors -->
\t    \t\t\t<!-- Product Summary & Options -->
\t    \t\t\t<div class=\"col-sm-6 product-details\">
\t    \t\t\t\t<h4>";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nomProjet", array()), "html", null, true);
        echo "</h4>
\t    \t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t<span class=\"price-was\"></span> ";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "budjet", array()), "html", null, true);
        echo " D
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h5>Bref description</h5>
\t    \t\t\t\t<p>";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "resume", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t\t<table class=\"shop-item-selections\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td> ça vous plais </td>
                                                                        
\t\t\t\t\t\t\t         <td>      <a href=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "idProjet", array()))), "html", null, true);
        echo "\">  Edit </a></td>
                                                                       <td>       ";
        // line 109
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "    </td>     
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</tr>
                                                        <tr>
                                                        <td> \t<a href=\"#\" class=\"btn btn\"><i class=\"icon-shopping-cart icon-white\"></i> Aider nous a demarer </a></td>
                                                            </tr>
                                                             <tr>
                                                            <td>  <a href=\"";
        // line 116
        echo $this->env->getExtension('routing')->getPath("projet");
        echo "\">Back to the list </a></td>
</tr>
\t\t\t\t\t\t</table>
\t    \t\t\t</div>
\t    \t\t\t<!-- End Product Summary & Options -->
\t    \t\t\t<!-- Full Description & Specification -->
\t    \t\t\t<div class=\"col-sm-12\">
\t    \t\t\t\t<div class=\"tabbable\">
\t    \t\t\t\t\t<!-- Tabs -->
\t\t\t\t\t\t\t<ul class=\"nav nav-tabs product-details-nav\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a href=\"#tab1\" data-toggle=\"tab\">Description</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#tab2\" data-toggle=\"tab\"> Commenter </a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t<!-- Tab Content (Full Description) -->
\t\t\t\t\t\t\t<div class=\"tab-content product-detail-info\">
\t\t\t\t\t\t\t\t<div class=\"tab-pane active\" id=\"tab1\">
\t\t\t\t\t\t\t\t\t<h4>Description du projet</h4>
\t\t\t\t\t\t\t\t\t<p>";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "resume", array()), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Tab Content (Specification) -->
\t\t\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab2\">
\t\t\t\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t\t\t\t\t    \t\t\t ";
        // line 139
        $this->loadTemplate("FOSCommentBundle:Thread:async.html.twig", "utilisateurBundle:Projet:show.html.twig", 139)->display(array_merge($context, array("id" => "foo")));
        // line 140
        echo "
                                                                                                                   
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
                                                                
                                                                
                                                                    
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t    \t\t\t</div>
\t    \t\t\t<!-- End Full Description & Specification -->
\t    \t\t</div>
\t\t\t</div>
\t\t</div>
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
";
    }

    // line 220
    public function block_javascripts($context, array $blocks = array())
    {
        // line 221
        echo "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js\"></script>



";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Projet:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 221,  310 => 220,  228 => 140,  226 => 139,  217 => 133,  197 => 116,  187 => 109,  183 => 108,  173 => 101,  167 => 98,  162 => 96,  145 => 83,  130 => 71,  111 => 55,  104 => 51,  96 => 46,  86 => 39,  79 => 35,  72 => 31,  65 => 27,  58 => 23,  51 => 19,  42 => 12,  39 => 11,  33 => 5,  30 => 4,  11 => 3,);
    }
}
/* */
/* */
/* {% extends "::base.html.twig" %}*/
/* {% block css %}*/
/* */
/* */
/* {% endblock%}*/
/* */
/* */
/* */
/* {% block body %}*/
/*     <!-- End Product Image & Available */
/*     <h1>Projet</h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Idprojet</th>*/
/*                 <td>{{ entity.idProjet }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nomprojet</th>*/
/*                 <td>{{ entity.nomProjet }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Resume</th>*/
/*                 <td>{{ entity.resume }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Image</th>*/
/*                 <td>{{ entity.image }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Budjet</th>*/
/*                 <td>{{ entity.budjet }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Fichier</th>*/
/*                 <td>{{ entity.fichier }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('projet') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>*/
/*         <a href="{{ path('projet_edit', { 'id': entity.idProjet }) }}">*/
/*             Edit*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/*     */
/*     Colors -->*/
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     <!-- Page Title -->*/
/* 		<div class="section section-breadcrumbs">*/
/* 			<div class="container">*/
/* 				<div class="row">*/
/* 					<div class="col-md-12">*/
/* 						<h1>{{ entity.nomProjet }}</h1>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*         */
/*         <div class="section">*/
/* 	    	<div class="container">*/
/* 	    		<div class="row">*/
/* 	    			<!-- Product Image & Available Colors -->*/
/* 	    			<div class="col-sm-6">*/
/* 	    				<div class="product-image-large">*/
/* 	    					<img src="{{asset('images/image/')}}{{entity.imageFile }}" alt="Item Name">*/
/* 	    				</div>*/
/* 	    				<div class="colors">*/
/* 							<span class="color-white"></span>*/
/* 							<span class="color-black"></span>*/
/* 							<span class="color-blue"></span>*/
/* 							<span class="color-orange"></span>*/
/* 							<span class="color-green"></span>*/
/* 						</div>*/
/* 	    			</div>*/
/* 	    			<!-- End Product Image & Available Colors -->*/
/* 	    			<!-- Product Summary & Options -->*/
/* 	    			<div class="col-sm-6 product-details">*/
/* 	    				<h4>{{ entity.nomProjet }}</h4>*/
/* 	    				<div class="price">*/
/* 							<span class="price-was"></span> {{ entity.budjet }} D*/
/* 						</div>*/
/* 						<h5>Bref description</h5>*/
/* 	    				<p>{{ entity.resume }}</p>*/
/* 						<table class="shop-item-selections">*/
/* 							*/
/* 							*/
/* 							<tr>*/
/* 								<td> ça vous plais </td>*/
/*                                                                         */
/* 							         <td>      <a href="{{ path('projet_edit', { 'id': entity.idProjet }) }}">  Edit </a></td>*/
/*                                                                        <td>       {{ form(delete_form) }}    </td>     */
/* 								*/
/* 							</tr>*/
/*                                                         <tr>*/
/*                                                         <td> 	<a href="#" class="btn btn"><i class="icon-shopping-cart icon-white"></i> Aider nous a demarer </a></td>*/
/*                                                             </tr>*/
/*                                                              <tr>*/
/*                                                             <td>  <a href="{{ path('projet') }}">Back to the list </a></td>*/
/* </tr>*/
/* 						</table>*/
/* 	    			</div>*/
/* 	    			<!-- End Product Summary & Options -->*/
/* 	    			<!-- Full Description & Specification -->*/
/* 	    			<div class="col-sm-12">*/
/* 	    				<div class="tabbable">*/
/* 	    					<!-- Tabs -->*/
/* 							<ul class="nav nav-tabs product-details-nav">*/
/* 								<li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>*/
/* 								<li><a href="#tab2" data-toggle="tab"> Commenter </a></li>*/
/* 							</ul>*/
/* 							<!-- Tab Content (Full Description) -->*/
/* 							<div class="tab-content product-detail-info">*/
/* 								<div class="tab-pane active" id="tab1">*/
/* 									<h4>Description du projet</h4>*/
/* 									<p>{{ entity.resume }}</p>*/
/* 									*/
/* 								</div>*/
/* 								<!-- Tab Content (Specification) -->*/
/* 								<div class="tab-pane" id="tab2">*/
/* 									<table>*/
/* 											    			 {% include 'FOSCommentBundle:Thread:async.html.twig' with {'id': 'foo'} %}*/
/* */
/*                                                                                                                    */
/* 									</table>*/
/* 								</div>*/
/*                                                                 */
/*                                                                 */
/*                                                                     */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* 							</div>*/
/* 						</div>*/
/* 	    			</div>*/
/* 	    			<!-- End Full Description & Specification -->*/
/* 	    		</div>*/
/* 			</div>*/
/* 		</div>*/
/*  */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>*/
/* */
/* */
/* */
/* {% endblock %}*/
/* */

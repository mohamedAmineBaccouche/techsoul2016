<?php

/* utilisateurBundle:Projet:index.html.twig */
class __TwigTemplate_333ce4ee8722d7d61354d4941f1879f7ce1f6ed51de68ea32b5acb95be649a25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Projet:index.html.twig", 1);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "

";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "    <!-- Text input
    <h1>Projet list</h1>

    <table class=\"records_list\">
        <thead>
            <tr>
                <th>Idprojet</th>
                <th>Nomprojet</th>
                <th>Resume</th>
                <th>Image</th>
                <th>Budjet</th>
                <th>Fichier</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 27
            echo "            <tr>
                <td><a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_show", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "idProjet", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nomProjet", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "resume", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "image", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "budjet", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "fichier", array()), "html", null, true);
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_show", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">show</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_edit", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">edit</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("projet_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
      
    
    
    
    -->
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    



    <!-- Page Title -->
    <div class=\"section section-breadcrumbs\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <h1>Visiter Nos Projets</h1>
                </div>
            </div>
        </div>
    </div>

    <div class=\"section\">
        <div class=\"container\">
            <div class=\"row\">
                <!-- Sidebar -->
                <div class=\"col-sm-3 blog-sidebar\">
                    <h4>chercher des projet</h4>
                                        ";
        // line 97
        echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("utilisateurBundle:Projet:recherche"), array());
        // line 98
        echo "
                   
                    
                  
                    <h4>Categories</h4>
                    <ul class=\"blog-categories\">
                        
                        ";
        // line 105
        echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('http_kernel')->controller("utilisateurBundle:Ctegorieprojet:menu"), array());
        // line 106
        echo "                        
                        
                        
                    
                </div>
                <!-- End Sidebar -->
                <div class=\"col-sm-8\">
                    
                    <ul class=\"thumbnails list-unstyled\">
                        
                        
                        
                        
                    ";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 120
            echo "  
                    
                       
                        
                        
        <li class=\"col-md-6\">
          <div class=\"thumbnail\" style=\"padding: 0\">
            <div style=\"padding:4px\">
                <img
                
               alt=\"300x200\" style=\"width: 100%\" src=\"";
            // line 130
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/image/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "imageFile", array()), "html", null, true);
            echo "\" >
              
            </div>
            <div class=\"caption\">
                
                
             <a href=\"";
            // line 136
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("projet_show", array("id" => $this->getAttribute($context["entity"], "idProjet", array()))), "html", null, true);
            echo "\">  <h2>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nomProjet", array()), "html", null, true);
            echo "</h2> </a>
            <p>";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "resume", array()), "html", null, true);
            echo "</p>
              
              
              
              <p><i class=\"icon icon-map-marker\"></i> Place, Country</p>
            </div>
            <div class=\"modal-footer\" style=\"text-align: left\">
              <div class=\"progress\">
                <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:  ";
            // line 145
            echo twig_escape_filter($this->env, (($this->getAttribute($context["entity"], "argent", array()) / $this->getAttribute($context["entity"], "budjet", array())) * 100), "html", null, true);
            echo "%;\">
                    <span class=\"sr-only\">60% Complete</span>
                </div>
              </div>
              <div class=\"row\">
                <div class=\"col-md-4\"><b>250%</b><br/><small>aquis</small></div>
                <div class=\"col-md-4\"><b>D";
            // line 151
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "budjet", array()), "html", null, true);
            echo "</b><br/><small>But</small></div>
                <div class=\"col-md-4\"><b>FUNDED</b><br/><small>AUG 5</small></div>
              </div>
            </div>
          </div>
        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo "
          
      </ul>

                </div>
            </div>
        </div>
    </div>

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
";
    }

    // line 208
    public function block_javascripts($context, array $blocks = array())
    {
        // line 209
        echo "


";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Projet:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 209,  315 => 208,  263 => 158,  250 => 151,  241 => 145,  230 => 137,  224 => 136,  214 => 130,  202 => 120,  198 => 119,  183 => 106,  181 => 105,  172 => 98,  170 => 97,  121 => 51,  114 => 46,  102 => 40,  96 => 37,  89 => 33,  85 => 32,  81 => 31,  77 => 30,  73 => 29,  67 => 28,  64 => 27,  60 => 26,  42 => 10,  39 => 9,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* {% block css %}*/
/* */
/* */
/* {% endblock%}*/
/* */
/* */
/* */
/* {% block body %}*/
/*     <!-- Text input*/
/*     <h1>Projet list</h1>*/
/* */
/*     <table class="records_list">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Idprojet</th>*/
/*                 <th>Nomprojet</th>*/
/*                 <th>Resume</th>*/
/*                 <th>Image</th>*/
/*                 <th>Budjet</th>*/
/*                 <th>Fichier</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td><a href="{{ path('projet_show', { 'id': entity.idProjet }) }}">{{ entity.idProjet }}</a></td>*/
/*                 <td>{{ entity.nomProjet }}</td>*/
/*                 <td>{{ entity.resume }}</td>*/
/*                 <td>{{ entity.image }}</td>*/
/*                 <td>{{ entity.budjet }}</td>*/
/*                 <td>{{ entity.fichier }}</td>*/
/*                 <td>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <a href="{{ path('projet_show', { 'id': entity.idProjet }) }}">show</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ path('projet_edit', { 'id': entity.idProjet }) }}">edit</a>*/
/*                     </li>*/
/*                 </ul>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul>*/
/*         <li>*/
/*             <a href="{{ path('projet_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*       */
/*     */
/*     */
/*     */
/*     -->*/
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/* */
/*     */
/*     */
/*     */
/*     */
/*     */
/* */
/* */
/* */
/*     <!-- Page Title -->*/
/*     <div class="section section-breadcrumbs">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-md-12">*/
/*                     <h1>Visiter Nos Projets</h1>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="section">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <!-- Sidebar -->*/
/*                 <div class="col-sm-3 blog-sidebar">*/
/*                     <h4>chercher des projet</h4>*/
/*                                         {% render(controller('utilisateurBundle:Projet:recherche'))%}*/
/* */
/*                    */
/*                     */
/*                   */
/*                     <h4>Categories</h4>*/
/*                     <ul class="blog-categories">*/
/*                         */
/*                         {%  render(controller('utilisateurBundle:Ctegorieprojet:menu'))  %}*/
/*                         */
/*                         */
/*                         */
/*                     */
/*                 </div>*/
/*                 <!-- End Sidebar -->*/
/*                 <div class="col-sm-8">*/
/*                     */
/*                     <ul class="thumbnails list-unstyled">*/
/*                         */
/*                         */
/*                         */
/*                         */
/*                     {% for entity in entities %}*/
/*   */
/*                     */
/*                        */
/*                         */
/*                         */
/*         <li class="col-md-6">*/
/*           <div class="thumbnail" style="padding: 0">*/
/*             <div style="padding:4px">*/
/*                 <img*/
/*                 */
/*                alt="300x200" style="width: 100%" src="{{asset('images/image/')}}{{entity.imageFile }}" >*/
/*               */
/*             </div>*/
/*             <div class="caption">*/
/*                 */
/*                 */
/*              <a href="{{ path('projet_show', { 'id': entity.idProjet }) }}">  <h2>{{ entity.nomProjet }}</h2> </a>*/
/*             <p>{{ entity.resume }}</p>*/
/*               */
/*               */
/*               */
/*               <p><i class="icon icon-map-marker"></i> Place, Country</p>*/
/*             </div>*/
/*             <div class="modal-footer" style="text-align: left">*/
/*               <div class="progress">*/
/*                 <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:  {{ entity.argent /  entity.budjet *100 }}%;">*/
/*                     <span class="sr-only">60% Complete</span>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="row">*/
/*                 <div class="col-md-4"><b>250%</b><br/><small>aquis</small></div>*/
/*                 <div class="col-md-4"><b>D{{ entity.budjet }}</b><br/><small>But</small></div>*/
/*                 <div class="col-md-4"><b>FUNDED</b><br/><small>AUG 5</small></div>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </li>*/
/*                     {% endfor %}*/
/* */
/*           */
/*       </ul>*/
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* */
/* */
/* */
/* {% endblock %}*/

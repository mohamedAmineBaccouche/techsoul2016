<?php

/* utilisateurBundle:Reclamation:index.html.twig */
class __TwigTemplate_009b0825a56e3d00762cca1987b6f151a6180e0917fc44e848b59dc401f49f14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Reclamation:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "<div class=\"row\">
          <br><br><br><br>
     </div>
    
    
    
    <h1 class=\"panel-title\">Liste des Reclamations</h1>
    <table id=\"shTable\" class=\"table table-striped table-bordered\">
        <thead class=\"\">
            <tr>
                <th>Titre</th>
                <th>Continue</th>
                <th>Notification</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 28
            echo "                <tr>
                    <td><a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("reclamation_show", array("id" => $this->getAttribute($context["entity"], "idReclamation", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "titre", array()), "html", null, true);
            echo "</a></td>
                    <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "contenu", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "notification", array()), "html", null, true);
            echo "</td>
                    <td>
                        <ul>
                            <li>
                                <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("reclamation_show", array("id" => $this->getAttribute($context["entity"], "idReclamation", array()))), "html", null, true);
            echo "\">show</a>
                            </li>
                            <li>
                                <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("reclamation_edit", array("id" => $this->getAttribute($context["entity"], "idReclamation", array()))), "html", null, true);
            echo "\">edit</a>
                            </li>
                        </ul>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "        </tbody>
    </table>
    <ul>
        <li>
            <a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("reclamation_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Reclamation:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 48,  92 => 44,  80 => 38,  74 => 35,  67 => 31,  63 => 30,  57 => 29,  54 => 28,  50 => 27,  31 => 10,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     */
/*     */
/*                                                                                                             */
/*                                                                                                   */
/*     */
/*     */
/*      <div class="row">*/
/*           <br><br><br><br>*/
/*      </div>*/
/*     */
/*     */
/*     */
/*     <h1 class="panel-title">Liste des Reclamations</h1>*/
/*     <table id="shTable" class="table table-striped table-bordered">*/
/*         <thead class="">*/
/*             <tr>*/
/*                 <th>Titre</th>*/
/*                 <th>Continue</th>*/
/*                 <th>Notification</th>*/
/*                 <th>Action</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*             {% for entity in entities %}*/
/*                 <tr>*/
/*                     <td><a href="{{ path('reclamation_show', { 'id': entity.idReclamation }) }}">{{ entity.titre }}</a></td>*/
/*                     <td>{{ entity.contenu }}</td>*/
/*                     <td>{{ entity.notification }}</td>*/
/*                     <td>*/
/*                         <ul>*/
/*                             <li>*/
/*                                 <a href="{{ path('reclamation_show', { 'id': entity.idReclamation }) }}">show</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="{{ path('reclamation_edit', { 'id': entity.idReclamation }) }}">edit</a>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('reclamation_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */

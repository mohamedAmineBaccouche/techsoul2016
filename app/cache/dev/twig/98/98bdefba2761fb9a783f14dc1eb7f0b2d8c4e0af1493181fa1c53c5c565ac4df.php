<?php

/* crowdBundle:Default/Projets:detail_projets.html.twig */
class __TwigTemplate_85b39a596e4c8f42d7165fa725a7cb68a6226f0772d04e2a16a25b0f398628dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("::base.html.twig", "crowdBundle:Default/Projets:detail_projets.html.twig", 3);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_css($context, array $blocks = array())
    {
        // line 5
        echo "

";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<!-- Page Title -->
\t\t<div class=\"section section-breadcrumbs\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h1>Product Details</h1>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
        
        <div class=\"section\">
\t    \t<div class=\"container\">
\t    \t\t<div class=\"row\">
\t    \t\t\t<!-- Product Image & Available Colors -->
\t    \t\t\t<div class=\"col-sm-6\">
\t    \t\t\t\t<div class=\"product-image-large\">
\t    \t\t\t\t\t<img src=\"img/product3.jpg\" alt=\"Item Name\">
\t    \t\t\t\t</div>
\t    \t\t\t\t<div class=\"colors\">
\t\t\t\t\t\t\t<span class=\"color-white\"></span>
\t\t\t\t\t\t\t<span class=\"color-black\"></span>
\t\t\t\t\t\t\t<span class=\"color-blue\"></span>
\t\t\t\t\t\t\t<span class=\"color-orange\"></span>
\t\t\t\t\t\t\t<span class=\"color-green\"></span>
\t\t\t\t\t\t</div>
\t    \t\t\t</div>
\t    \t\t\t<!-- End Product Image & Available Colors -->
\t    \t\t\t<!-- Product Summary & Options -->
\t    \t\t\t<div class=\"col-sm-6 product-details\">
\t    \t\t\t\t<h4>LOREM IPSUM DOLOR</h4>
\t    \t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t<span class=\"price-was\">\$959.99</span> \$999.99
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h5>Quick Overview</h5>
\t    \t\t\t\t<p>
\t    \t\t\t\t\tMorbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat massa ornare vitae. Ut fermentum justo vel venenatis eleifend. Fusce id magna eros.
\t    \t\t\t\t</p>
\t\t\t\t\t\t<table class=\"shop-item-selections\">
\t\t\t\t\t\t\t<!-- Color Selector -->
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><b>Color:</b></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<div class=\"dropdown choose-item-color\">
\t\t\t\t\t\t\t\t\t\t<a class=\"btn btn-sm btn-grey\" data-toggle=\"dropdown\" href=\"#\"><span class=\"color-orange\"></span> Orange <b class=\"caret\"></b></a>
\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\"><span class=\"color-white\"></span> White</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\"><span class=\"color-black\"></span> Black</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\"><span class=\"color-blue\"></span> Blue</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\"><span class=\"color-orange\"></span> Orange</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\"><span class=\"color-green\"></span> Green</a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<!-- Size Selector -->
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><b>Size:</b></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<div class=\"dropdown\">
\t\t\t\t\t\t\t\t\t\t<a class=\"btn btn-sm btn-grey\" data-toggle=\"dropdown\" href=\"#\">XXL <b class=\"caret\"></b></a>
\t\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\">XS</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\">S</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\">M</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\">L</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li role=\"menuitem\"><a href=\"#\">XXL</a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<!-- Quantity -->
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><b>Quantity:</b></td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control input-sm input-micro\" value=\"1\">
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<!-- Add to Cart Button -->
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td>&nbsp;</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn\"><i class=\"icon-shopping-cart icon-white\"></i> Add to Cart</a>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</table>
\t    \t\t\t</div>
\t    \t\t\t<!-- End Product Summary & Options -->
\t    \t\t\t
\t    \t\t\t<!-- Full Description & Specification -->
\t    \t\t\t<div class=\"col-sm-12\">
\t    \t\t\t\t<div class=\"tabbable\">
\t    \t\t\t\t\t<!-- Tabs -->
\t\t\t\t\t\t\t<ul class=\"nav nav-tabs product-details-nav\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a href=\"#tab1\" data-toggle=\"tab\">Description</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#tab2\" data-toggle=\"tab\">Specification</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t<!-- Tab Content (Full Description) -->
\t\t\t\t\t\t\t<div class=\"tab-content product-detail-info\">
\t\t\t\t\t\t\t\t<div class=\"tab-pane active\" id=\"tab1\">
\t\t\t\t\t\t\t\t\t<h4>Product Description</h4>
\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\tDonec hendrerit massa metus, a ultrices elit iaculis eu. Pellentesque ullamcorper augue lacus. Phasellus et est quis diam iaculis fringilla id nec sapien. Sed tempor ornare felis, non vulputate dolor. Etiam ornare diam vitae ligula malesuada tempor. Vestibulum nec odio vel libero ullamcorper euismod et in sapien. Suspendisse potenti.
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t<h4>Product Highlights</h4>
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li>Nullam dictum augue nec iaculis rhoncus. Aenean lobortis fringilla orci, vitae varius purus eleifend vitae.</li>
\t\t\t\t\t\t\t\t\t\t<li>Nunc ornare, dolor a ultrices ultricies, magna dolor convallis enim, sed volutpat quam sem sed tellus.</li>
\t\t\t\t\t\t\t\t\t\t<li>Aliquam malesuada cursus urna a rutrum. Ut ultricies facilisis suscipit.</li>
\t\t\t\t\t\t\t\t\t\t<li>Duis a magna iaculis, aliquam metus in, luctus eros.</li>
\t\t\t\t\t\t\t\t\t\t<li>Aenean nisi nibh, imperdiet sit amet eleifend et, gravida vitae sem.</li>
\t\t\t\t\t\t\t\t\t\t<li>Donec quis nisi congue, ultricies massa ut, bibendum velit.</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<h4>Usage Information</h4>
\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\tDonec hendrerit massa metus, a ultrices elit iaculis eu. Pellentesque ullamcorper augue lacus. Phasellus et est quis diam iaculis fringilla id nec sapien. Sed tempor ornare felis, non vulputate dolor. Etiam ornare diam vitae ligula malesuada tempor. Vestibulum nec odio vel libero ullamcorper euismod et in sapien. Suspendisse potenti.
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Tab Content (Specification) -->
\t\t\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab2\">
\t\t\t\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>Total sensor Pixels (megapixels)</td>
\t\t\t\t\t\t\t\t\t\t\t<td>Approx. 16.7</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>Effective Pixels (megapixels)</td>
\t\t\t\t\t\t\t\t\t\t\t<td>Approx. 16.1</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>Automatic White Balance</td>
\t\t\t\t\t\t\t\t\t\t\t<td>YES</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>White balance: preset selection</td>
\t\t\t\t\t\t\t\t\t\t\t<td>Daylight, Shade, Cloudy, Incandescent, Fluorescent, Flash</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>White balance: custom setting</td>
\t\t\t\t\t\t\t\t\t\t\t<td>YES</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>White balance: types of color temperature</td>
\t\t\t\t\t\t\t\t\t\t\t<td>YES (G7 to M7,15-step) (A7 to B7,15-step)</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>White balance bracketing</td>
\t\t\t\t\t\t\t\t\t\t\t<td>NO</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td>ISO Sensitivity Setting</td>
\t\t\t\t\t\t\t\t\t\t\t<td>ISO100 - 25600 equivalent</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t    \t\t\t</div>
\t    \t\t\t<!-- End Full Description & Specification -->
\t    \t\t</div>
\t\t\t</div>
\t\t</div>
";
    }

    // line 176
    public function block_javascripts($context, array $blocks = array())
    {
        // line 177
        echo "


";
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/Projets:detail_projets.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 177,  208 => 176,  42 => 12,  39 => 11,  33 => 5,  30 => 4,  11 => 3,);
    }
}
/* */
/* */
/* {% extends "::base.html.twig" %}*/
/* {% block css %}*/
/* */
/* */
/* {% endblock%}*/
/* */
/* */
/* */
/* {% block body %}*/
/* <!-- Page Title -->*/
/* 		<div class="section section-breadcrumbs">*/
/* 			<div class="container">*/
/* 				<div class="row">*/
/* 					<div class="col-md-12">*/
/* 						<h1>Product Details</h1>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*         */
/*         <div class="section">*/
/* 	    	<div class="container">*/
/* 	    		<div class="row">*/
/* 	    			<!-- Product Image & Available Colors -->*/
/* 	    			<div class="col-sm-6">*/
/* 	    				<div class="product-image-large">*/
/* 	    					<img src="img/product3.jpg" alt="Item Name">*/
/* 	    				</div>*/
/* 	    				<div class="colors">*/
/* 							<span class="color-white"></span>*/
/* 							<span class="color-black"></span>*/
/* 							<span class="color-blue"></span>*/
/* 							<span class="color-orange"></span>*/
/* 							<span class="color-green"></span>*/
/* 						</div>*/
/* 	    			</div>*/
/* 	    			<!-- End Product Image & Available Colors -->*/
/* 	    			<!-- Product Summary & Options -->*/
/* 	    			<div class="col-sm-6 product-details">*/
/* 	    				<h4>LOREM IPSUM DOLOR</h4>*/
/* 	    				<div class="price">*/
/* 							<span class="price-was">$959.99</span> $999.99*/
/* 						</div>*/
/* 						<h5>Quick Overview</h5>*/
/* 	    				<p>*/
/* 	    					Morbi eleifend congue elit nec sagittis. Praesent aliquam lobortis tellus, nec consequat massa ornare vitae. Ut fermentum justo vel venenatis eleifend. Fusce id magna eros.*/
/* 	    				</p>*/
/* 						<table class="shop-item-selections">*/
/* 							<!-- Color Selector -->*/
/* 							<tr>*/
/* 								<td><b>Color:</b></td>*/
/* 								<td>*/
/* 									<div class="dropdown choose-item-color">*/
/* 										<a class="btn btn-sm btn-grey" data-toggle="dropdown" href="#"><span class="color-orange"></span> Orange <b class="caret"></b></a>*/
/* 										<ul class="dropdown-menu" role="menu">*/
/* 											<li role="menuitem"><a href="#"><span class="color-white"></span> White</a></li>*/
/* 											<li role="menuitem"><a href="#"><span class="color-black"></span> Black</a></li>*/
/* 											<li role="menuitem"><a href="#"><span class="color-blue"></span> Blue</a></li>*/
/* 											<li role="menuitem"><a href="#"><span class="color-orange"></span> Orange</a></li>*/
/* 											<li role="menuitem"><a href="#"><span class="color-green"></span> Green</a></li>*/
/* 										</ul>*/
/* 									</div>*/
/* 								</td>*/
/* 							</tr>*/
/* 							<!-- Size Selector -->*/
/* 							<tr>*/
/* 								<td><b>Size:</b></td>*/
/* 								<td>*/
/* 									<div class="dropdown">*/
/* 										<a class="btn btn-sm btn-grey" data-toggle="dropdown" href="#">XXL <b class="caret"></b></a>*/
/* 										<ul class="dropdown-menu" role="menu">*/
/* 											<li role="menuitem"><a href="#">XS</a></li>*/
/* 											<li role="menuitem"><a href="#">S</a></li>*/
/* 											<li role="menuitem"><a href="#">M</a></li>*/
/* 											<li role="menuitem"><a href="#">L</a></li>*/
/* 											<li role="menuitem"><a href="#">XXL</a></li>*/
/* 										</ul>*/
/* 									</div>*/
/* 								</td>*/
/* 							</tr>*/
/* 							<!-- Quantity -->*/
/* 							<tr>*/
/* 								<td><b>Quantity:</b></td>*/
/* 								<td>*/
/* 									<input type="text" class="form-control input-sm input-micro" value="1">*/
/* 								</td>*/
/* 							</tr>*/
/* 							<!-- Add to Cart Button -->*/
/* 							<tr>*/
/* 								<td>&nbsp;</td>*/
/* 								<td>*/
/* 									<a href="#" class="btn btn"><i class="icon-shopping-cart icon-white"></i> Add to Cart</a>*/
/* 								</td>*/
/* 							</tr>*/
/* 						</table>*/
/* 	    			</div>*/
/* 	    			<!-- End Product Summary & Options -->*/
/* 	    			*/
/* 	    			<!-- Full Description & Specification -->*/
/* 	    			<div class="col-sm-12">*/
/* 	    				<div class="tabbable">*/
/* 	    					<!-- Tabs -->*/
/* 							<ul class="nav nav-tabs product-details-nav">*/
/* 								<li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>*/
/* 								<li><a href="#tab2" data-toggle="tab">Specification</a></li>*/
/* 							</ul>*/
/* 							<!-- Tab Content (Full Description) -->*/
/* 							<div class="tab-content product-detail-info">*/
/* 								<div class="tab-pane active" id="tab1">*/
/* 									<h4>Product Description</h4>*/
/* 									<p>*/
/* 										Donec hendrerit massa metus, a ultrices elit iaculis eu. Pellentesque ullamcorper augue lacus. Phasellus et est quis diam iaculis fringilla id nec sapien. Sed tempor ornare felis, non vulputate dolor. Etiam ornare diam vitae ligula malesuada tempor. Vestibulum nec odio vel libero ullamcorper euismod et in sapien. Suspendisse potenti.*/
/* 									</p>*/
/* 									<h4>Product Highlights</h4>*/
/* 									<ul>*/
/* 										<li>Nullam dictum augue nec iaculis rhoncus. Aenean lobortis fringilla orci, vitae varius purus eleifend vitae.</li>*/
/* 										<li>Nunc ornare, dolor a ultrices ultricies, magna dolor convallis enim, sed volutpat quam sem sed tellus.</li>*/
/* 										<li>Aliquam malesuada cursus urna a rutrum. Ut ultricies facilisis suscipit.</li>*/
/* 										<li>Duis a magna iaculis, aliquam metus in, luctus eros.</li>*/
/* 										<li>Aenean nisi nibh, imperdiet sit amet eleifend et, gravida vitae sem.</li>*/
/* 										<li>Donec quis nisi congue, ultricies massa ut, bibendum velit.</li>*/
/* 									</ul>*/
/* 									<h4>Usage Information</h4>*/
/* 									<p>*/
/* 										Donec hendrerit massa metus, a ultrices elit iaculis eu. Pellentesque ullamcorper augue lacus. Phasellus et est quis diam iaculis fringilla id nec sapien. Sed tempor ornare felis, non vulputate dolor. Etiam ornare diam vitae ligula malesuada tempor. Vestibulum nec odio vel libero ullamcorper euismod et in sapien. Suspendisse potenti.*/
/* 									</p>*/
/* 								</div>*/
/* 								<!-- Tab Content (Specification) -->*/
/* 								<div class="tab-pane" id="tab2">*/
/* 									<table>*/
/* 										<tr>*/
/* 											<td>Total sensor Pixels (megapixels)</td>*/
/* 											<td>Approx. 16.7</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>Effective Pixels (megapixels)</td>*/
/* 											<td>Approx. 16.1</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>Automatic White Balance</td>*/
/* 											<td>YES</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>White balance: preset selection</td>*/
/* 											<td>Daylight, Shade, Cloudy, Incandescent, Fluorescent, Flash</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>White balance: custom setting</td>*/
/* 											<td>YES</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>White balance: types of color temperature</td>*/
/* 											<td>YES (G7 to M7,15-step) (A7 to B7,15-step)</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>White balance bracketing</td>*/
/* 											<td>NO</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td>ISO Sensitivity Setting</td>*/
/* 											<td>ISO100 - 25600 equivalent</td>*/
/* 										</tr>*/
/* 									</table>*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* 	    			</div>*/
/* 	    			<!-- End Full Description & Specification -->*/
/* 	    		</div>*/
/* 			</div>*/
/* 		</div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* */
/* */
/* */
/* {% endblock %}*/

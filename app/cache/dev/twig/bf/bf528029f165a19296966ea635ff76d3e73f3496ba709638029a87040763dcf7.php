<?php

/* ::forumbase.html.twig */
class __TwigTemplate_f52a16a45b0256bb695638f2e194febc17ff884b7df3bbacf6d8527b6602d7f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'navbar' => array($this, 'block_navbar'),
            'tool_bar' => array($this, 'block_tool_bar'),
            'hn' => array($this, 'block_hn'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
       
        
                <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width\">

        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/icomoon-social.css"), "html", null, true);
        echo "\">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>

        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.css"), "html", null, true);
        echo "\" />
\t\t<!--[if lte IE 8]>
\t\t    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/leaflet.ie.css"), "html", null, true);
        echo "\" />
\t\t<![endif]-->
\t\t<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">

        <script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr-2.6.2-respond-1.1.0.min.js"), "html", null, true);
        echo "\"></script>
        
        ";
        // line 25
        $this->displayBlock('css', $context, $blocks);
        // line 27
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("path/to/css/fileinput.min.css"), "html", null, true);
        echo "\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

      

\t\t<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\">
\t\t
    </head>
    <body>
        
        ";
        // line 39
        $this->loadTemplate("::navbar.html.twig", "::forumbase.html.twig", 39)->display($context);
        // line 44
        $this->displayBlock('navbar', $context, $blocks);
        // line 50
        $this->displayBlock('hn', $context, $blocks);
        // line 56
        echo "        
        
        
        ";
        // line 59
        $this->loadTemplate("::footer.html.twig", "::forumbase.html.twig", 59)->display($context);
        // line 60
        echo "        
        
        ";
        // line 62
        $this->displayBlock('javascripts', $context, $blocks);
        // line 64
        echo "          <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}\"></script>
        <script>window.jQuery || document.write('<script src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.min.js"), "html", null, true);
        echo "\"><\\/script>')</script>
        <script src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js')}}\"></script>
        <script src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.fitvids.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.sequence-min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.bxslider.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/main-menu.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/template.js"), "html", null, true);
        echo "\"></script>
        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>
<!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.
     This must be loaded before fileinput.min.js -->
<script src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("path/to/js/plugins/canvas-to-blob.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("path/to/js/fileinput.min.js"), "html", null, true);
        echo "\"></script>
<!-- bootstrap.js below is only needed if you wish to the feature of viewing details
     of text file preview via modal dialog -->
<script src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<!-- optionally if you need translation for your language then include 
    locale file as mentioned below -->
<script src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("path/to/js/fileinput_locale_<lang>.js"), "html", null, true);
        echo "\"></script>
      <script>
    \$('#file-fr').fileinput({
        language: 'fr',
        uploadUrl: '#',
        allowedFileExtensions : ['jpg', 'png','gif'],
    });
    \$('#file-es').fileinput({
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions : ['jpg', 'png','gif'],
    });
    \$(\"#file-0\").fileinput({
        'allowedFileExtensions' : ['jpg', 'png','gif'],
    });
    \$(\"#file-1\").fileinput({
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions : ['jpg', 'png','gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
\t});
    /*
    \$(\".file\").on('fileselect', function(event, n, l) {
        alert('File Selected. Name: ' + l + ', Num: ' + n);
    });
    */
\t\$(\"#file-3\").fileinput({
\t\tshowUpload: false,
\t\tshowCaption: false,
\t\tbrowseClass: \"btn btn-primary btn-lg\",
\t\tfileType: \"any\",
        previewFileIcon: \"<i class='glyphicon glyphicon-king'></i>\"
\t});
\t\$(\"#file-4\").fileinput({
\t\tuploadExtraData: {kvId: '10'}
\t});
    \$(\".btn-warning\").on('click', function() {
        if (\$('#file-4').attr('disabled')) {
            \$('#file-4').fileinput('enable');
        } else {
            \$('#file-4').fileinput('disable');
        }
    });    
    \$(\".btn-info\").on('click', function() {
        \$('#file-4').fileinput('refresh', {previewClass:'bg-info'});
    });
    /*
    \$('#file-4').on('fileselectnone', function() {
        alert('Huh! You selected no files.');
    });
    \$('#file-4').on('filebrowse', function() {
        alert('File browse clicked for #file-4');
    });
    */
    \$(document).ready(function() {
        \$(\"#test-upload\").fileinput({
            'showPreview' : false,
            'allowedFileExtensions' : ['jpg', 'png','gif'],
            'elErrorContainer': '#errorBlock'
        });
        /*
        \$(\"#test-upload\").on('fileloaded', function(event, file, previewId, index) {
            alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
        });
        */
    });
\t</script>  
        
        
    </body>
</html>
";
    }

    // line 25
    public function block_css($context, array $blocks = array())
    {
        // line 26
        echo "        ";
    }

    // line 44
    public function block_navbar($context, array $blocks = array())
    {
        $this->displayBlock('tool_bar', $context, $blocks);
    }

    public function block_tool_bar($context, array $blocks = array())
    {
    }

    // line 50
    public function block_hn($context, array $blocks = array())
    {
        // line 51
        echo "        
        
        
        
        ";
    }

    // line 62
    public function block_javascripts($context, array $blocks = array())
    {
        // line 63
        echo "        ";
    }

    public function getTemplateName()
    {
        return "::forumbase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 63,  271 => 62,  263 => 51,  260 => 50,  250 => 44,  246 => 26,  243 => 25,  162 => 83,  156 => 80,  150 => 77,  146 => 76,  139 => 72,  135 => 71,  131 => 70,  127 => 69,  123 => 68,  118 => 66,  114 => 65,  111 => 64,  109 => 62,  105 => 60,  103 => 59,  98 => 56,  96 => 50,  94 => 44,  92 => 39,  79 => 29,  75 => 28,  70 => 27,  68 => 25,  63 => 23,  58 => 21,  53 => 19,  48 => 17,  42 => 14,  38 => 13,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*        */
/*         */
/*                 <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">*/
/*         <title>mPurpose - Multipurpose Feature Rich Bootstrap Template</title>*/
/*         <meta name="description" content="">*/
/*         <meta name="viewport" content="width=device-width">*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">*/
/*         <link rel="stylesheet" href="{{asset('css/icomoon-social.css')}}">*/
/*         <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>*/
/* */
/*         <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" />*/
/* 		<!--[if lte IE 8]>*/
/* 		    <link rel="stylesheet" href="{{asset('css/leaflet.ie.css')}}" />*/
/* 		<![endif]-->*/
/* 		<link rel="stylesheet" href="{{asset('css/main.css')}}">*/
/* */
/*         <script src="{{asset('js/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>*/
/*         */
/*         {% block css %}*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*         <link href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css')}}" rel="stylesheet">*/
/* <link href="{{asset('path/to/css/fileinput.min.css')}}" media="all" rel="stylesheet" type="text/css" />*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*       */
/* */
/* 		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">*/
/* 		*/
/*     </head>*/
/*     <body>*/
/*         */
/*         {% include '::navbar.html.twig' %}*/
/*         */
/*        */
/*         */
/*         */
/*         {%- block navbar -%}{%- block tool_bar -%}*/
/* 					{%- endblock tool_bar -%}*/
/* 				{%- endblock navbar -%}*/
/*         */
/*    */
/*         */
/*           {% block hn %}*/
/*         */
/*         */
/*         */
/*         */
/*         {% endblock %}*/
/*         */
/*         */
/*         */
/*         {% include '::footer.html.twig' %}*/
/*         */
/*         */
/*         {% block javascripts %}*/
/*         {% endblock %}*/
/*           <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js')}}"></script>*/
/*         <script>window.jQuery || document.write('<script src="{{asset('js/jquery-1.9.1.min.js')}}"><\/script>')</script>*/
/*         <script src="{{asset('js/bootstrap.min.js')}}"></script>*/
/*         <script src="http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.fitvids.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.sequence-min.js')}}"></script>*/
/*         <script src="{{asset('js/jquery.bxslider.js')}}"></script>*/
/*         <script src="{{asset('js/main-menu.js')}}"></script>*/
/*         <script src="{{asset('js/template.js')}}"></script>*/
/*         <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>*/
/* <!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.*/
/*      This must be loaded before fileinput.min.js -->*/
/* <script src="{{asset('path/to/js/plugins/canvas-to-blob.min.js')}}" type="text/javascript"></script>*/
/* <script src="{{asset('path/to/js/fileinput.min.js')}}"></script>*/
/* <!-- bootstrap.js below is only needed if you wish to the feature of viewing details*/
/*      of text file preview via modal dialog -->*/
/* <script src="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js')}}" type="text/javascript"></script>*/
/* <!-- optionally if you need translation for your language then include */
/*     locale file as mentioned below -->*/
/* <script src="{{asset('path/to/js/fileinput_locale_<lang>.js')}}"></script>*/
/*       <script>*/
/*     $('#file-fr').fileinput({*/
/*         language: 'fr',*/
/*         uploadUrl: '#',*/
/*         allowedFileExtensions : ['jpg', 'png','gif'],*/
/*     });*/
/*     $('#file-es').fileinput({*/
/*         language: 'es',*/
/*         uploadUrl: '#',*/
/*         allowedFileExtensions : ['jpg', 'png','gif'],*/
/*     });*/
/*     $("#file-0").fileinput({*/
/*         'allowedFileExtensions' : ['jpg', 'png','gif'],*/
/*     });*/
/*     $("#file-1").fileinput({*/
/*         uploadUrl: '#', // you must set a valid URL here else you will get an error*/
/*         allowedFileExtensions : ['jpg', 'png','gif'],*/
/*         overwriteInitial: false,*/
/*         maxFileSize: 1000,*/
/*         maxFilesNum: 10,*/
/*         //allowedFileTypes: ['image', 'video', 'flash'],*/
/*         slugCallback: function(filename) {*/
/*             return filename.replace('(', '_').replace(']', '_');*/
/*         }*/
/* 	});*/
/*     /**/
/*     $(".file").on('fileselect', function(event, n, l) {*/
/*         alert('File Selected. Name: ' + l + ', Num: ' + n);*/
/*     });*/
/*     *//* */
/* 	$("#file-3").fileinput({*/
/* 		showUpload: false,*/
/* 		showCaption: false,*/
/* 		browseClass: "btn btn-primary btn-lg",*/
/* 		fileType: "any",*/
/*         previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"*/
/* 	});*/
/* 	$("#file-4").fileinput({*/
/* 		uploadExtraData: {kvId: '10'}*/
/* 	});*/
/*     $(".btn-warning").on('click', function() {*/
/*         if ($('#file-4').attr('disabled')) {*/
/*             $('#file-4').fileinput('enable');*/
/*         } else {*/
/*             $('#file-4').fileinput('disable');*/
/*         }*/
/*     });    */
/*     $(".btn-info").on('click', function() {*/
/*         $('#file-4').fileinput('refresh', {previewClass:'bg-info'});*/
/*     });*/
/*     /**/
/*     $('#file-4').on('fileselectnone', function() {*/
/*         alert('Huh! You selected no files.');*/
/*     });*/
/*     $('#file-4').on('filebrowse', function() {*/
/*         alert('File browse clicked for #file-4');*/
/*     });*/
/*     *//* */
/*     $(document).ready(function() {*/
/*         $("#test-upload").fileinput({*/
/*             'showPreview' : false,*/
/*             'allowedFileExtensions' : ['jpg', 'png','gif'],*/
/*             'elErrorContainer': '#errorBlock'*/
/*         });*/
/*         /**/
/*         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {*/
/*             alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);*/
/*         });*/
/*         *//* */
/*     });*/
/* 	</script>  */
/*         */
/*         */
/*     </body>*/
/* </html>*/
/* */

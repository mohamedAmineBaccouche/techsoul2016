<?php

/* crowdBundle:Default/Contact:contact.html.twig */
class __TwigTemplate_c422a7f69bc64f9115e4a318a9853d72d64286c6d41e0576bf4d207100271a4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("::base.html.twig", "crowdBundle:Default/Contact:contact.html.twig", 4);
        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_css($context, array $blocks = array())
    {
        // line 6
        echo "



";
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "    <!-- Page Title -->
    <div class=\"section section-breadcrumbs\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-6\">
                    <h1>Contact Us</h1>
                </div>
            </div>
        </div>
    </div>

    <div class=\"section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-7\">
                    <!-- Map -->
                    <div id=\"contact-us-map\">

                    </div>
                    <!-- End Map -->
                    <!-- Contact Info -->
                    <p class=\"contact-us-details\">
                        <b>Address:</b> 123 Fake Street, LN1 2ST, London, United Kingdom<br/>
                        <b>Phone:</b> +44 123 654321<br/>
                        <b>Fax:</b> +44 123 654321<br/>
                        <b>Email:</b> <a href=\"mailto:getintoutch@yourcompanydomain.com\">getintoutch@yourcompanydomain.com</a>
                    </p>
                    <!-- End Contact Info -->
                </div>
                <div class=\"col-sm-5\">
                    <!-- Contact Form -->
                    <h3>Send Us a Message</h3>
                    <div class=\"contact-form-wrapper\">
                        <form class=\"form-horizontal\" role=\"form\">
                            <div class=\"form-group\">
                                <label for=\"Name\" class=\"col-sm-3 control-label\"><b>Your name</b></label>
                                <div class=\"col-sm-9\">
                                    <input class=\"form-control\" id=\"Name\" type=\"text\" placeholder=\"\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"contact-email\" class=\"col-sm-3 control-label\"><b>Your Email</b></label>
                                <div class=\"col-sm-9\">
                                    <input class=\"form-control\" id=\"contact-email\" type=\"text\" placeholder=\"\">
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"contact-message\" class=\"col-sm-3 control-label\"><b>Select Topic</b></label>
                                <div class=\"col-sm-9\">
                                    <select class=\"form-control\" id=\"prependedInput\">
                                        <option>Please select topic...</option>
                                        <option>General</option>
                                        <option>Services</option>
                                        <option>Orders</option>
                                    </select>
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"contact-message\" class=\"col-sm-3 control-label\"><b>Message</b></label>
                                <div class=\"col-sm-9\">
                                    <textarea class=\"form-control\" rows=\"5\" id=\"contact-message\"></textarea>
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn pull-right\">Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Contact Info -->
                </div>
            </div>
        </div>
    </div>
    ";
    }

    // line 92
    public function block_javascripts($context, array $blocks = array())
    {
        // line 93
        echo "


";
    }

    public function getTemplateName()
    {
        return "crowdBundle:Default/Contact:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 93,  123 => 92,  44 => 15,  41 => 14,  33 => 6,  30 => 5,  11 => 4,);
    }
}
/* */
/* */
/* */
/* {% extends "::base.html.twig" %}*/
/* {% block css %}*/
/* */
/* */
/* */
/* */
/* {% endblock%}*/
/* */
/* */
/* */
/* {% block body %}*/
/*     <!-- Page Title -->*/
/*     <div class="section section-breadcrumbs">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-md-6">*/
/*                     <h1>Contact Us</h1>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="section">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-sm-7">*/
/*                     <!-- Map -->*/
/*                     <div id="contact-us-map">*/
/* */
/*                     </div>*/
/*                     <!-- End Map -->*/
/*                     <!-- Contact Info -->*/
/*                     <p class="contact-us-details">*/
/*                         <b>Address:</b> 123 Fake Street, LN1 2ST, London, United Kingdom<br/>*/
/*                         <b>Phone:</b> +44 123 654321<br/>*/
/*                         <b>Fax:</b> +44 123 654321<br/>*/
/*                         <b>Email:</b> <a href="mailto:getintoutch@yourcompanydomain.com">getintoutch@yourcompanydomain.com</a>*/
/*                     </p>*/
/*                     <!-- End Contact Info -->*/
/*                 </div>*/
/*                 <div class="col-sm-5">*/
/*                     <!-- Contact Form -->*/
/*                     <h3>Send Us a Message</h3>*/
/*                     <div class="contact-form-wrapper">*/
/*                         <form class="form-horizontal" role="form">*/
/*                             <div class="form-group">*/
/*                                 <label for="Name" class="col-sm-3 control-label"><b>Your name</b></label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <input class="form-control" id="Name" type="text" placeholder="">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="contact-email" class="col-sm-3 control-label"><b>Your Email</b></label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <input class="form-control" id="contact-email" type="text" placeholder="">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="contact-message" class="col-sm-3 control-label"><b>Select Topic</b></label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <select class="form-control" id="prependedInput">*/
/*                                         <option>Please select topic...</option>*/
/*                                         <option>General</option>*/
/*                                         <option>Services</option>*/
/*                                         <option>Orders</option>*/
/*                                     </select>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <label for="contact-message" class="col-sm-3 control-label"><b>Message</b></label>*/
/*                                 <div class="col-sm-9">*/
/*                                     <textarea class="form-control" rows="5" id="contact-message"></textarea>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <div class="col-sm-12">*/
/*                                     <button type="submit" class="btn pull-right">Send</button>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </form>*/
/*                     </div>*/
/*                     <!-- End Contact Info -->*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* */
/* */
/* */
/* {% endblock %}*/

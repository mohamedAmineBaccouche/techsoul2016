<?php

/* utilisateurBundle:Comment:index.html.twig */
class __TwigTemplate_b7a802cca4ce02febe8e32841e8ec3386e1a8d422b2042fb9f72d0ef44183999 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Comment:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"section section-breadcrumbs\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h1> Listes Commentaires </h1>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
    
    
    
    
<div class=\"container\">
 
  <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("comment_new");
        echo "\">
                Create a new entry
            </a>
  
  
  <table class=\"table table-striped\">
    <thead>
      <tr>
        <th> ID </th>
        <th>Score</th>
        <th>commentaire</th>
        <th>Actions</th>
     
      </tr>
    </thead>
    <tbody>
         ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 37
            echo "      <tr>
        <td><a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("comment_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</a></td>
      <td>  ";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "score", array()), "html", null, true);
            echo "</td>
      <td>  ";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "body", array()), "html", null, true);
            echo "</td>
      
        <td><a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("comment_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">show</a></td>
        <td> <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("comment_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">edit</a></td>
      </tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "    </tbody>
  </table>
</div>

    
    
    
    
    
    
    
    
    
    
    
    ";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Comment:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 46,  93 => 43,  89 => 42,  84 => 40,  80 => 39,  74 => 38,  71 => 37,  67 => 36,  48 => 20,  31 => 5,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     */
/*     	<div class="section section-breadcrumbs">*/
/* 			<div class="container">*/
/* 				<div class="row">*/
/* 					<div class="col-md-12">*/
/* 						<h1> Listes Commentaires </h1>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*     */
/*     */
/*     */
/*     */
/* <div class="container">*/
/*  */
/*   <a href="{{ path('comment_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*   */
/*   */
/*   <table class="table table-striped">*/
/*     <thead>*/
/*       <tr>*/
/*         <th> ID </th>*/
/*         <th>Score</th>*/
/*         <th>commentaire</th>*/
/*         <th>Actions</th>*/
/*      */
/*       </tr>*/
/*     </thead>*/
/*     <tbody>*/
/*          {% for entity in entities %}*/
/*       <tr>*/
/*         <td><a href="{{ path('comment_show', { 'id': entity.id }) }}">{{ entity.id }}</a></td>*/
/*       <td>  {{ entity.score }}</td>*/
/*       <td>  {{ entity.body }}</td>*/
/*       */
/*         <td><a href="{{ path('comment_show', { 'id': entity.id }) }}">show</a></td>*/
/*         <td> <a href="{{ path('comment_edit', { 'id': entity.id }) }}">edit</a></td>*/
/*       </tr>*/
/*       {% endfor %}*/
/*     </tbody>*/
/*   </table>*/
/* </div>*/
/* */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     {% endblock %}*/
/* */

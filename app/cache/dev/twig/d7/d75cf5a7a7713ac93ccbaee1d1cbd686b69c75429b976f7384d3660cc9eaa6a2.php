<?php

/* utilisateurBundle:Message:Repondre.html.twig */
class __TwigTemplate_35fdd496c3e3f363ad14e8f121711d1952134ae58a19400997cf3cb480efd4a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "utilisateurBundle:Message:Repondre.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'css' => array($this, 'block_css'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayBlock('css', $context, $blocks);
        // line 6
        echo "

    <div class=\"container\">
        <div class=\"row profile\">
            <div class=\"col-md-3\">
                <div class=\"profile-sidebar\">
                    <!-- SIDEBAR USERPIC -->
                    <div class=\"profile-userpic\">
                        <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/test/avatar.png"), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"\">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class=\"profile-usertitle\">
                        <div class=\"profile-usertitle-name\">
                            ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "
                        </div>
                        <div class=\"profile-usertitle-job\">
                            Developer
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class=\"profile-userbuttons\">
                        <button type=\"button\" class=\"btn btn-success btn-sm\">Follow</button>
                        <button type=\"button\" class=\"btn btn-danger btn-sm\">Message</button>
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class=\"profile-usermenu\">
                        <ul class=\"nav\">
                            <li class=\"active\">
                                <a href=\"#\">
                                    <i class=\"glyphicon glyphicon-home\"></i>
                                    Overview </a>
                            </li>
                            <li>
                                <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
        echo "\">
                                    <i class=\"glyphicon glyphicon-user\"></i>
                                    parametre du compte  </a>
                            </li>
                            <li>
                                <a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("mesprojet");
        echo "\">
                                    <i class=\"glyphicon glyphicon-ok\"  ></i>
                                    mes projett </a>
                            </li>
                            <li>
                                <a href=\"\">
                                    <i class=\"glyphicon glyphicon glyphicon-envelope\"  ></i>
                                    mes message </a>
                            </li>
                            <li>
                                <a href=\"\">
                                    <i class=\"glyphicon glyphicon glyphicon-envelope\"  ></i>
                                    composer message </a>
                            </li>
                            <li>
                                <a href=\"#\">
                                    <i class=\"glyphicon glyphicon-flag\"></i>
                                    Help </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>

            <div class=\"col-md-9\">
                <div class=\"profile-content\">


    <h1>Message creation</h1>

    ";
        // line 78
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("message");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>


                </div>
            </div>
        </div>

        <br>
        <br>
    ";
    }

    // line 3
    public function block_css($context, array $blocks = array())
    {
        // line 4
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/test/test.css"), "html", null, true);
        echo "\">
    ";
    }

    public function getTemplateName()
    {
        return "utilisateurBundle:Message:Repondre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 4,  146 => 3,  128 => 82,  121 => 78,  87 => 47,  79 => 42,  54 => 20,  45 => 14,  35 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* {% block body %}*/
/*     {% block css %}*/
/*         <link rel="stylesheet" href="{{asset('css/test/test.css')}}">*/
/*     {% endblock %}*/
/* */
/* */
/*     <div class="container">*/
/*         <div class="row profile">*/
/*             <div class="col-md-3">*/
/*                 <div class="profile-sidebar">*/
/*                     <!-- SIDEBAR USERPIC -->*/
/*                     <div class="profile-userpic">*/
/*                         <img src="{{asset('css/test/avatar.png')}}" class="img-responsive" alt="">*/
/*                     </div>*/
/*                     <!-- END SIDEBAR USERPIC -->*/
/*                     <!-- SIDEBAR USER TITLE -->*/
/*                     <div class="profile-usertitle">*/
/*                         <div class="profile-usertitle-name">*/
/*                             {{ user.username }}*/
/*                         </div>*/
/*                         <div class="profile-usertitle-job">*/
/*                             Developer*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- END SIDEBAR USER TITLE -->*/
/*                     <!-- SIDEBAR BUTTONS -->*/
/*                     <div class="profile-userbuttons">*/
/*                         <button type="button" class="btn btn-success btn-sm">Follow</button>*/
/*                         <button type="button" class="btn btn-danger btn-sm">Message</button>*/
/*                     </div>*/
/*                     <!-- END SIDEBAR BUTTONS -->*/
/*                     <!-- SIDEBAR MENU -->*/
/*                     <div class="profile-usermenu">*/
/*                         <ul class="nav">*/
/*                             <li class="active">*/
/*                                 <a href="#">*/
/*                                     <i class="glyphicon glyphicon-home"></i>*/
/*                                     Overview </a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="{{path('fos_user_profile_edit')}}">*/
/*                                     <i class="glyphicon glyphicon-user"></i>*/
/*                                     parametre du compte  </a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="{{path('mesprojet')}}">*/
/*                                     <i class="glyphicon glyphicon-ok"  ></i>*/
/*                                     mes projett </a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="">*/
/*                                     <i class="glyphicon glyphicon glyphicon-envelope"  ></i>*/
/*                                     mes message </a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="">*/
/*                                     <i class="glyphicon glyphicon glyphicon-envelope"  ></i>*/
/*                                     composer message </a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="#">*/
/*                                     <i class="glyphicon glyphicon-flag"></i>*/
/*                                     Help </a>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </div>*/
/*                     <!-- END MENU -->*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="col-md-9">*/
/*                 <div class="profile-content">*/
/* */
/* */
/*     <h1>Message creation</h1>*/
/* */
/*     {{ form(form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('message') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* */
/* */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <br>*/
/*         <br>*/
/*     {% endblock %}*/
/* */
